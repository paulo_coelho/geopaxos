# Geographic State Machine Replication

This repository implements GeoPaxos.

## Description

Many current online services need to serve clients distributed across geographic areas. These systems are subject to stringent availability and performance requirements. In order to meet these requirements, replication is used to tolerate the crash of servers and improve performance by deploying replicas near the clients. Coordinating geographically distributed replicas, however, is challenging. This paper presents GeoPaxos, a protocol that addresses this challenge by combining three insights. It decouples order from execution in state machine replication, it induces a partial order on the execution of operations, instead of a total order, and it exploits geographic locality, typical of geo-distributed online services. GeoPaxos outperforms state-of- the-art approaches by more than an order of magnitude in some cases. We describe GeoPaxos design and implementation in detail, and present an extensive performance evaluation.

More information about GeoPaxos can be found [here](http://web.inf.usi.ch/file/pub/102/geopaxos.pdf)

## Compiling

GeoPaxos depends on [libpaxos](https://bitbucket.org/sciascid/libpaxos/src/master/) and [libmcast](https://bitbucket.org/paulo_coelho/libmcast/src/master/).
After installing such libraries, just `cd` into the root folder and run:

```
mkdir build && cd build
cmake ..
make
```

## Running

We can run a sample client/server following these steps:

```
./run-2g3p.sh         # from libmcast
./geo-kvs-server -h   # exhibits help: check parameters and set them accordingly
./geo-kvs-client -h   # exhibits help: check parameters and set them accordingly
<<<<<<< HEAD
```
