/*
 * Copyright (c) 2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "buffer.h"
#include "log.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct buf_t
{
  mcast_message msg;
  char          data[MAX_DATA_SIZE];
  const void*   ptr;
};

struct buffer_t
{
  struct buf_t*    buf;
  long             capacity;
  long             size;
  long             head, tail;
  int              full, empty;
  pthread_mutex_t* mut;
  pthread_cond_t * not_full, *not_empty;
};

static void buffer_add(struct buffer_t* b, mcast_message* m, const void* ptr);
static void buffer_del(struct buffer_t* b, mcast_message* out,
                       const void** ptr);

struct buffer_t*
buffer_new(long capacity)
{
  struct buffer_t* b;
  long             i;

  b = malloc(sizeof(struct buffer_t));
  b->capacity = capacity;
  b->buf = malloc(capacity * sizeof(struct buf_t));
  memset(b->buf, 0, capacity * sizeof(struct buf_t));
  for (i = 0; i < b->capacity; i++)
    b->buf[i].msg.value.mcast_value_val = b->buf[i].data;
  b->empty = 1;
  b->full = 0;
  b->head = 0;
  b->tail = 0;
  b->mut = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
  pthread_mutex_init(b->mut, NULL);
  b->not_full = (pthread_cond_t*)malloc(sizeof(pthread_cond_t));
  pthread_cond_init(b->not_full, NULL);
  b->not_empty = (pthread_cond_t*)malloc(sizeof(pthread_cond_t));
  pthread_cond_init(b->not_empty, NULL);
  return b;
}

void
buffer_free(struct buffer_t* b)
{
  pthread_mutex_destroy(b->mut);
  free(b->mut);
  pthread_cond_destroy(b->not_full);
  free(b->not_full);
  pthread_cond_destroy(b->not_empty);
  free(b->not_empty);
  free(b->buf);
  free(b);
}

void
buffer_push(struct buffer_t* b, mcast_message* m, const void* ptr)
{
  LOG_DEBUG("BUFFER: ...pushing");
  pthread_mutex_lock(b->mut);
  while (b->full)
    pthread_cond_wait(b->not_full, b->mut);
  buffer_add(b, m, ptr);
  pthread_mutex_unlock(b->mut);
  pthread_cond_signal(b->not_empty);
  LOG_DEBUG("BUFFER: pushed...");
}

void
buffer_pop(struct buffer_t* b, mcast_message* data_ptr, const void** ptr_ptr)
{
  LOG_DEBUG("BUFFER: popping...");
  pthread_mutex_lock(b->mut);
  while (b->empty)
    pthread_cond_wait(b->not_empty, b->mut);

  buffer_del(b, data_ptr, ptr_ptr);
  pthread_mutex_unlock(b->mut);
  pthread_cond_signal(b->not_full);
  LOG_DEBUG("BUFFER: ...popped");
}

long
buffer_size(struct buffer_t* b)
{
  return b->size;
}

/* STATIC FUNCTIONS */
static void
buffer_add(struct buffer_t* b, mcast_message* m, const void* ptr)
{
  size_t len = m->value.mcast_value_len;
  if (len > MAX_DATA_SIZE) {
    LOG_ERROR("BUFFER: data is bigger than MAX_DATA_SIZE (%zu / %d)", len,
              MAX_DATA_SIZE);
    len = MAX_DATA_SIZE;
  }

  mcast_message_copy(&b->buf[b->tail].msg, m, 0);
  b->buf[b->tail].msg.value.mcast_value_len = len;
  b->buf[b->tail].msg.value.mcast_value_val = b->buf[b->tail].data;
  memcpy(b->buf[b->tail].msg.value.mcast_value_val, m->value.mcast_value_val,
         len);
  b->buf[b->tail].ptr = ptr;
  b->tail = (b->tail + 1) % b->capacity;
  b->empty = 0;
  b->size++;
  if (b->tail == b->head)
    b->full = 1;
}

static void
buffer_del(struct buffer_t* b, mcast_message* out, const void** ptr)
{
  mcast_message_copy(out, &b->buf[b->head].msg, 0);
  out->value.mcast_value_len = b->buf[b->head].msg.value.mcast_value_len;
  memcpy(out->value.mcast_value_val, b->buf[b->head].msg.value.mcast_value_val,
         out->value.mcast_value_len);
  *ptr = b->buf[b->head].ptr;
  b->head = (b->head + 1) % b->capacity;
  b->full = 0;
  b->size--;
  if (b->head == b->tail)
    b->empty = 1;
}
