/*
 * Copyright (c) 2017-2018, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "geopaxos.h"
#include "barrier.h"
#include "buffer.h"
#include "log.h"
#include "thpool/thpool.h"
#include "uthash.h"
#include <assert.h>
#include <errno.h>
#include <evamcast.h>
#include <event2/buffer.h>
#include <event2/event.h>
#include <event2/thread.h>
#include <evmcast.h>
#include <inttypes.h>
#include <math.h>
#include <mcast.h>
#include <message_mcast.h>
#include <netinet/tcp.h>
#include <peers_mcast.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define QUEUE_SIZE 10000

/* Represent a client connected to a GeoPaxos replica */
struct connected_client
{
  unsigned            unique_client_id; // the hashtable key
  struct bufferevent* bev;              // client endpoint
  UT_hash_handle      hh;               // hashtable handler (used by uthash)
};

/* Thread that consumes and executes operations ordered within a group */
struct consumer
{
  pthread_t        t;        // the thread
  unsigned         group;    // group id
  struct geopaxos* parent;   // parent replica
  struct buffer_t* buf;      // buffer of ordered operations
  long             count;    // size of the buffer
  char*            response; // response to the client
};

/* Thread that sends operations to be ordered by the destination group(s).
 * Enqueues ordered operations to be executed. */
struct producer
{
  pthread_t              t;      // the thread
  unsigned               group;  // group id
  unsigned               node;   // node id
  struct geopaxos*       parent; // parent replica
  struct buffer_t*       buf;    // buffer of ordered operations
  struct event_base*     base;   // used by group constructor
  struct mcast_replica*  mr;     // listener of ordered operations
  struct evmcast_config* conf;   // groups configuration
};

/* Main GeoPaxos replica */
struct geopaxos
{
  unsigned                 id;          // replica id
  unsigned                 my_group;    // replica's main group
  unsigned                 group_count; // total number of groups
  const char*              conf;        // path to configuration file
  struct mcast_peers*      peers;    // server to receive operations from client
  struct event_base*       base;     // manages server related event
  struct connected_client* clients;  // connected clients hashtable
  struct barrier_t**       barriers; // barriers for multi-group operations
  destination_cb   d_cb;   // callback for destination from operation arguments
  execute_cb       e_cb;   // callback to execute ordered operations
  void*            cb_arg; // additional argument to be passed to callbacks
  struct producer* prod;   // producers array, one per group
  struct consumer* cons;   // consumers array, one per group
  threadpool       thpool; // thread pool to define destination
  struct buffer_t* mbuf;   // client messages buffer
};

/*********** STATIC FIELDS AND FUNCTIONS *************/
static int      stop_consuming = 0;
static void*    consume(void*);
static void*    produce(void*);
static unsigned bit_count(unsigned);
static void     client_handler(void*, int);
static void     handle_clients(struct mcast_peer*, mcast_message*, void*);
static struct connected_client* get_client(struct connected_client**, unsigned);
static int add_client(struct connected_client**, unsigned, struct bufferevent*);
static void del_client_all(struct connected_client**);

/**********************************************
 * GEOPAXOS (Replica)  v v v v v v v v v      *
 **********************************************/
/**************** SERVER SIDE ***************/
void
geopaxos_reply(struct geopaxos* g, unsigned client_id, const char* response,
               unsigned len)
{
  struct connected_client* cc = get_client(&g->clients, client_id);
  mcast_message            m;

  if (cc) {
    m.uid = client_id;
    m.from_group = g->my_group;
    m.from_node = g->id;
    m.to_groups_len = 0;
    m.type = MCAST_DELIVERED;
    send_mcast_message(cc->bev, &m);
  } else
    LOG_ERROR("No client with id %u", client_id);
}

struct geopaxos*
geopaxos_init2(unsigned my_group, unsigned my_id, unsigned* nids,
               unsigned nids_len, const char* libmcast_config,
               struct event_base* base)
{
  int                    i = 0, count = 0;
  struct geopaxos*       g;
  struct evmcast_config* config = evmcast_config_read(libmcast_config);

  if (config == NULL)
    return NULL;

  g = malloc(sizeof(struct geopaxos));
  memset(g, 0, sizeof(struct geopaxos));
  g->id = my_id;
  g->my_group = my_group;
  g->group_count = evmcast_group_count(config);
  g->base = base;
  g->conf = libmcast_config;
  g->clients = NULL; // required by UTHASH
  g->mbuf = buffer_new(QUEUE_SIZE);
  g->cons = malloc(g->group_count * sizeof(struct consumer));
  g->prod = malloc(g->group_count * sizeof(struct producer));
  memset(g->cons, 0, g->group_count * sizeof(struct consumer));
  memset(g->prod, 0, g->group_count * sizeof(struct producer));

  /* initialize barriers: need to have one barrier per destination possibility,
   * this means 2^N, where N is the number of groups.
   *
   * A barrier index is defined by the destination groups:
   * e.g. operations addressed to groups 0 and 2, will use
   * barrier[i], with i = 2^0 + 2^2 = 5
   **/
  g->barriers = malloc(pow(2, g->group_count) * sizeof(struct barrier_t*));
  for (i = 0; i < pow(2, g->group_count); i++) {
    count = bit_count(i);
    g->barriers[i] = count > 1 ? barrier_new(count, i) : NULL;
  }

  // one pair consumer/producer per group
  for (i = 0; i < g->group_count; i++) {
    // consumer
    g->cons[i].group = i;
    g->cons[i].parent = g;
    g->cons[i].buf = buffer_new(QUEUE_SIZE);
    g->cons[i].count = 0;
    // producer
    g->prod[i].group = i;
    g->prod[i].node = nids_len > i ? nids[i] : 0;
    g->prod[i].parent = g;
    g->prod[i].buf = g->cons[i].buf;
    g->prod[i].conf = config;
  }

  evthread_use_pthreads();
  for (i = 0; i < g->group_count; i++) {
    pthread_create(&g->prod[i].t, NULL, produce, &g->prod[i]);
    pthread_create(&g->cons[i].t, NULL, consume, &g->cons[i]);
  }

  return g;
}

struct geopaxos*
geopaxos_init(unsigned my_group, unsigned my_id, const char* libmcast_config,
              struct event_base* base)
{
  return geopaxos_init2(my_group, my_id, 0, 0, libmcast_config, base);
}

unsigned
geopaxos_run(struct geopaxos* g, unsigned port, destination_cb d, execute_cb e,
             void* cb_arg)
{
  if (!d || !e) {
    LOG_ERROR("Callbacks can NOT be empty.");
    return 0;
  }

  g->peers = mcast_peers_new_mt(g->base, NULL);
  mcast_peers_subscribe(g->peers, MCAST_CLIENT, handle_clients, g);
  g->d_cb = d;
  g->e_cb = e;
  g->cb_arg = cb_arg;
  return mcast_peers_listen(g->peers, port);
}

unsigned
geopaxos_run_parallel(struct geopaxos* g, unsigned port, destination_cb d,
                      execute_cb e, void* cb_arg)
{
  int i;

  g->thpool = thpool_init(g->group_count);
  for (i = 0; i < g->group_count; ++i)
    thpool_add_work(g->thpool, client_handler, g);

  return geopaxos_run(g, port, d, e, cb_arg);
}

void
geopaxos_stop(struct geopaxos* g)
{
  int           i;
  mcast_message m;

  m.value.mcast_value_len = 0;
  if (g->peers)
    mcast_peers_free(g->peers);

  stop_consuming = 1;
  evmcast_config_free(g->prod[0].conf);
  for (i = 0; i < g->group_count; i++) {
    event_base_loopbreak(g->prod[i].base);
    if (g->prod[i].mr != NULL)
      mcast_replica_free(g->prod[i].mr);

    event_base_free(g->prod[i].base);
  }

  for (i = 0; i < pow(2, g->group_count); i++)
    if (bit_count(i) > 1)
      barrier_free(g->barriers[i]);

  for (i = 0; i < g->group_count; i++) {
    pthread_join(g->prod[i].t, NULL);
    buffer_push(g->cons[i].buf, &m, NULL);
    pthread_join(g->cons[i].t, NULL);
    buffer_free(g->cons[i].buf);
  }

  free(g->barriers);
  free(g->prod);
  free(g->cons);
  del_client_all(&g->clients);
  HASH_CLEAR(hh, g->clients);

  if (g->thpool)
    thpool_destroy(g->thpool);

  buffer_free(g->mbuf);
  free(g);
}

unsigned
geopaxos_group_count(struct geopaxos* g)
{
  return g->group_count;
}

/**********************************************
 * CLIENT-RELATED TASKS    v v v v v v v      *
 **********************************************/
static int
add_client(struct connected_client** clients, unsigned unique_client_id,
           struct bufferevent* bev)
{
  struct connected_client* cc;
  HASH_FIND_INT(*clients, &unique_client_id,
                cc); /* unique_client_id already in the hash? */
  if (cc == NULL) {
    cc = malloc(sizeof(struct connected_client));
    cc->unique_client_id = unique_client_id;
    cc->bev = bev;
    LOG_DEBUG("adding client %u", unique_client_id);
    HASH_ADD_INT(*clients, unique_client_id,
                 cc); /* unique_client_id: name of key field */
    return 1;
  }
  return 0;
}

static struct connected_client*
get_client(struct connected_client** clients, unsigned unique_client_id)
{
  struct connected_client* cc = NULL;
  HASH_FIND_INT(*clients, &unique_client_id,
                cc); /* unique_client_id already in the hash? */
  LOG_DEBUG("searching client %u with pointer %p, bev %p", unique_client_id, cc,
            (cc ? cc->bev : NULL));
  return cc;
}

static void
del_client_all(struct connected_client** clients)
{
  struct connected_client *cc, *tmp;
  HASH_ITER(hh, *clients, cc, tmp)
  {
    HASH_DEL(*clients, cc); /* delete; clients advances to next */
    if (cc->bev)
      bufferevent_free(cc->bev);

    free(cc); /* optional- if you want to free  */
  }
}

static void
client_handler(void* arg, int tid)
{
  struct geopaxos* g = arg;
  mcast_message    m;
  char             data[MAX_DATA_SIZE];
  const void*      ptr;

  m.value.mcast_value_val = data;
  while (!stop_consuming) {
    buffer_pop(g->mbuf, &m, &ptr);
    LOG_DEBUG("Handling message from client %" PRIu64 " in thread %d (%u)",
              m.uid, tid, (unsigned)pthread_self());
    m.to_groups_len = g->d_cb(g, m.uid, m.value.mcast_value_val,
                              m.value.mcast_value_len, m.to_groups, g->cb_arg);
    if (m.to_groups_len) {
      mcast_replica_submit_parallel(g->prod[m.to_groups[0]].mr, &m, tid);
    } else
      LOG_ERROR("Destination groups not set accordingly.");
  }
}

static void
handle_clients(struct mcast_peer* p, mcast_message* m, void* arg)
{
  struct geopaxos* g = arg;
  mcast_message    m2;
  int              size = m->value.mcast_value_len;
  char             buf[MAX_DATA_SIZE + sizeof(m_uid_t)];

  add_client(&g->clients, m->uid, mcast_peer_get_buffer(p));
  mcast_message_copy(&m2, m, 0);
  memcpy(buf, m->value.mcast_value_val, size);
  memcpy(&buf[size], &m->uid, sizeof(m_uid_t));
  m2.value.mcast_value_val = buf;
  m2.value.mcast_value_len = size + sizeof(m_uid_t);
  if (g->thpool != NULL)
    buffer_push(g->mbuf, &m2, NULL);
  else {
    m2.to_groups_len = g->d_cb(g, m2.uid, m2.value.mcast_value_val, size,
                               m2.to_groups, g->cb_arg);
    if (m2.to_groups_len)
      mcast_replica_submit(g->prod[m2.to_groups[0]].mr, &m2);
    else
      LOG_ERROR("Destination groups not set accordingly.");
  }
}

/**********************************************
 * PRODUCER  v v v v v v v v v v v v v v      *
 **********************************************/
/* Callback invoked when connected to a node in a group */
static void
on_connect(struct mcast_replica* mr, void* arg)
{
  struct producer* p = arg;

  LOG_INFO("Connected to group %d...\n", p->group);
}

/* Callback used whenever an operation has been ordered for a group */
static void
on_read(struct mcast_replica* mr, mcast_message* msg, void* arg)
{
  struct producer*         p = arg;
  m_uid_t                  client_id = 0;
  struct connected_client* c = NULL;

  // prepares the data and store it in the buffer
  memcpy(
    &client_id,
    &msg->value.mcast_value_val[msg->value.mcast_value_len - sizeof(m_uid_t)],
    sizeof(m_uid_t));
  LOG_DEBUG("Buffering ordered message from client %" PRIu64 " with %d bytes",
            client_id, msg->value.mcast_value_len);
  c = get_client(&p->parent->clients, client_id);
  buffer_push(p->buf, msg, c);
}

/* Producer thread main function: connects to its own group and waits for
 * ordered messages */
static void*
produce(void* arg)
{
  struct producer* p = arg;
  evthread_use_pthreads();
  p->base = event_base_new();
  LOG_INFO("Connecting to node %u in group %u...", p->node, p->group);
  p->mr = mcast_replica_connect(p->group, p->node, p->parent->conf, p->base,
                                on_connect, on_read, p);
  event_base_dispatch(p->base);
  printf("PRODUCER %d: exiting...\n", p->group);
  pthread_exit(NULL);
}

/**********************************************
 * CONSUMER  v v v v v v v v v v v v v v      *
 **********************************************/
/* Runs command, collects and sends reply to client */
static inline void
run_command(struct consumer* c, mcast_message* m, struct connected_client* cc)
{
  unsigned      client_id = cc ? cc->unique_client_id : 0, response_len = 0;
  mcast_message aux;

  LOG_DEBUG("Running command for client %u, bev %p", client_id,
            cc ? cc->bev : NULL);
  response_len =
    c->parent->e_cb(c->parent, client_id, m->from_group, m->to_groups,
                    m->to_groups_len, m->value.mcast_value_val,
                    m->value.mcast_value_len, &c->response, c->parent->cb_arg);

  if (cc) {
    mcast_message_copy(&aux, m, 0);
    aux.value.mcast_value_len = response_len;
    aux.value.mcast_value_val = c->response;
    LOG_DEBUG("Sending message to client %u...", cc->unique_client_id);
    send_mcast_message(cc->bev, &aux);
  }
}

/* Main consumer thread:
 * gets message from the buffer,
 * synchronizes with additional consumer from the operation destination,
 * and makes sure the operation is executed only once.
 **/
static void*
consume(void* arg)
{
  struct consumer*         c = arg;
  struct connected_client* cc = NULL;
  mcast_message            m = { 0 };
  char                     data[MAX_DATA_SIZE] = { 0 };
  struct barrier_t*        b = NULL;
  unsigned                 i = 0, barrier_id = 0;

  m.value.mcast_value_val = data;
  while (!stop_consuming) {
    buffer_pop(c->buf, &m, (const void**)&cc);
    if (stop_consuming)
      break;

    LOG_DEBUG("CONSUMER %d: received (%u, %" PRIu64
              "), |dst| = %d from multicast.",
              c->group, m.timestamp, m.uid, m.to_groups_len);

    barrier_id = 0;
    if (m.to_groups_len > 1) {
      for (i = 0; i < m.to_groups_len; i++) {
        barrier_id |= (1 << m.to_groups[i]);
      }

      LOG_DEBUG("CONSUMER %d: Getting barrier %u.", c->group, barrier_id);
      b = c->parent->barriers[barrier_id];
      barrier_step1(b);
      LOG_DEBUG("CONSUMER %d: passed barrier for message (%u, %" PRIu64 ")",
                c->group, m.timestamp, m.uid);
      i = barrier_lock_cs(b);
      if (i == 1) {
        LOG_DEBUG("CONSUMER %d: FIRST for message (%u, %" PRIu64
                  ") inside critical region.",
                  c->group, m.timestamp, m.uid);
        run_command(c, &m, cc);
      } else {
        LOG_DEBUG("CONSUMER %d: SKIPPING message (%u, %" PRIu64
                  ") inside critical region.",
                  c->group, m.timestamp, m.uid);
      }

      barrier_unlock_cs(b);
      barrier_step2(b);
    } else {
      LOG_DEBUG("CONSUMER %d: running SINGLE partition message (%u, %" PRIu64
                ").",
                c->group, m.timestamp, m.uid);
      run_command(c, &m, cc);
    }

    c->count++;
  }

  pthread_exit(NULL);
}

static unsigned
bit_count(unsigned n)
{
  unsigned counter = 0;
  while (n) {
    counter += n % 2;
    n >>= 1;
  }
  return counter;
}
