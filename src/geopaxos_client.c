/*
 * Copyright (c) 2016-2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "geopaxos.h"
#include "log.h"
#include <arpa/inet.h>
#include <errno.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/event.h>
#include <mcast.h>
#include <message_mcast.h>
#include <netinet/tcp.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Represents the client connection to a GeoPaxos replica */
struct geoclient
{
  unsigned                   uid; // client unique id
  struct bufferevent*        bev;
  geoclient_callback         rcb;
  geoclient_connect_callback ccb;
  void*                      cb_arg;
};

/* STATIC FUNCTIONS */
static void on_client_connect(struct bufferevent* bev, short events, void* arg);
static void on_client_read(struct bufferevent* bev, void* arg);
static void socket_set_nodelay(int fd);
static struct sockaddr_in address_to_sockaddr(const char* ip, unsigned port);

/*
 * Connects to a GeoPaxos server and register the response callback
 */
struct geoclient*
geopaxos_client_connect(const char* ip, unsigned port,
                        geoclient_connect_callback ccb, geoclient_callback cb,
                        void* cb_arg, struct event_base* base)
{
  struct sockaddr_in addr = address_to_sockaddr(ip, port);
  struct geoclient*  c = NULL;
  static int         first_time = 1;
  struct timespec    nanos;

  if (first_time) {
    clock_gettime(CLOCK_MONOTONIC, &nanos);
    srandom(nanos.tv_nsec);
    first_time = 0;
  }

  c = malloc(sizeof(struct geoclient));
  c->bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE);
  c->uid = random() & 0xffffffff;
  c->rcb = cb;
  c->ccb = ccb;
  c->cb_arg = cb_arg;
  bufferevent_setcb(c->bev, on_client_read, NULL, on_client_connect, c);
  bufferevent_enable(c->bev, EV_READ | EV_WRITE);
  bufferevent_socket_connect(c->bev, (struct sockaddr*)&addr, sizeof(addr));
  socket_set_nodelay(bufferevent_getfd(c->bev));
  return c;
}

/*
 * Submits a message to GeoPaxos infrastructure
 */
void
geopaxos_client_submit(struct geoclient* c, const char* msg, size_t len)
{
  mcast_message m;
  memset(&m, 0, sizeof(mcast_message));
  LOG_DEBUG("Client %u: submit new message of size %zu.", c->uid, len);
  m.uid = c->uid;
  m.value.mcast_value_len = len;
  m.value.mcast_value_val = (char*)msg;
  m.type = MCAST_CLIENT;
  send_mcast_message(c->bev, &m);
}

/*
 * Finishes connection and frees allocated memory
 */
void
geopaxos_client_finish(struct geoclient* c)
{
  bufferevent_free(c->bev);
  free(c);
}

unsigned
geopaxos_client_get_uid(struct geoclient* c)
{
  return c->uid;
}

/* STATIC FUNCTIONS */
static void
socket_set_nodelay(int fd)
{
  int flag = 1;
  setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(int));
}

/* Libevent read callback: receives and unserializes the response from the
 *replica, invokes the registered client's receive callback for each response.
 **/
static void
on_client_read(struct bufferevent* bev, void* arg)
{
  struct geoclient* c = arg;
  mcast_message     msg;
  struct evbuffer*  in = bufferevent_get_input(bev);

  while (recv_mcast_message(in, &msg)) {
    c->rcb(c, msg.value.mcast_value_val, msg.value.mcast_value_len, c->cb_arg);
    mcast_message_content_free(&msg);
  }
}

/* Libevent connect callback: invokes client's registered callback on success */
static void
on_client_connect(struct bufferevent* bev, short events, void* arg)
{
  struct geoclient* c = arg;

  if (events & BEV_EVENT_CONNECTED) {
    LOG_INFO("Client %u: connected to node.", c->uid);
    c->ccb(c, c->cb_arg);
  } else {
    LOG_ERROR("Client %u: socket ERROR: %s.", c->uid,
              evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
  }
}

static struct sockaddr_in
address_to_sockaddr(const char* ip, unsigned port)
{
  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(struct sockaddr_in));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = inet_addr(ip);
  return addr;
}
