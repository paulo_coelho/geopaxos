/*
 * Copyright (c) 2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "barrier.h"
#include "log.h"
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>

struct barrier_t
{ // two-phase barrier
  unsigned        buid;
  char            name1[32];
  char            name2[32];
  sem_t*          turnstile1;
  sem_t*          turnstile2;
  pthread_mutex_t mut;
  pthread_mutex_t cs_mut;
  unsigned        N;
  unsigned        count;
  unsigned        cs_count;
};

struct barrier_t*
barrier_new(int N, unsigned barrier_uid)
{
  struct barrier_t* b = malloc(sizeof(struct barrier_t));

  sprintf(b->name1, "/turnstile1-%u", barrier_uid);
  sprintf(b->name2, "/turnstile2-%u", barrier_uid);
  sem_unlink(b->name1);
  sem_unlink(b->name2);
  b->buid = barrier_uid;
  b->turnstile1 = sem_open(b->name1, O_CREAT, 0644, 0);
  b->turnstile2 = sem_open(b->name2, O_CREAT, 0644, 1);
  pthread_mutex_init(&b->mut, NULL);
  pthread_mutex_init(&b->cs_mut, NULL);
  b->N = N;
  b->count = 0;
  b->cs_count = 0;
  return b;
}

void
barrier_free(struct barrier_t* b)
{
  sem_close(b->turnstile1);
  sem_close(b->turnstile2);
  sem_unlink(b->name1);
  sem_unlink(b->name2);
  pthread_mutex_destroy(&b->mut);
  pthread_mutex_destroy(&b->cs_mut);
  free(b);
}

void
barrier_step1(struct barrier_t* b)
{
  pthread_mutex_lock(&b->mut);
  ++(b->count);
  LOG_DEBUG("BARRIER %u - STEP 1: thread %u of %u", b->buid, b->count, b->N);
  if (b->count == b->N) {
    sem_wait(b->turnstile2); // lock the second
    sem_post(b->turnstile1); // unlock the first
  }

  pthread_mutex_unlock(&b->mut);

  sem_wait(b->turnstile1); // first turnstile
  sem_post(b->turnstile1);
  LOG_DEBUG("BARRIER %u: passed STEP 1", b->buid);
}

void
barrier_step2(struct barrier_t* b)
{
  pthread_mutex_lock(&b->mut);
  LOG_DEBUG("BARRIER %u - STEP 2: thread %u of %u", b->buid, b->count, b->N);
  --(b->count);
  if (b->count == 0) {
    sem_wait(b->turnstile1); // lock the first
    sem_post(b->turnstile2); // unlock the second
  }

  pthread_mutex_unlock(&b->mut);

  sem_wait(b->turnstile2); // second turnstile
  sem_post(b->turnstile2);
  LOG_DEBUG("BARRIER %u: passed STEP 2", b->buid);
}

unsigned
barrier_lock_cs(struct barrier_t* b)
{
  pthread_mutex_lock(&b->cs_mut);
  ++(b->cs_count);
  return b->cs_count;
}

void
barrier_unlock_cs(struct barrier_t* b)
{
  if (b->cs_count == b->N)
    b->cs_count = 0;
  pthread_mutex_unlock(&b->cs_mut);
}
