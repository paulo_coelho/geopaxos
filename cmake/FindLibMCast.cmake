# - Find libmcast
# Find the native Message Pack headers and libraries.
#
#  LibMCast_INCLUDE_DIRS - where to find mcast.h, etc.
#  LibMCast_LIBRARIES    - List of libraries when using LibMCast.
#  LibMCast_FOUND        - True if LibMCast found.

# Look for the header file.
FIND_PATH(LibMCast_INCLUDE_DIR NAMES evamcast.h evmcast.h mcast.h
  HINTS "${LIBMCAST_ROOT}/include")

# Look for the library.

FIND_LIBRARY(LibMCast_LIBRARY NAMES evmcast
  HINTS "${LIBMCAST_ROOT}/lib")

FIND_LIBRARY(LibAMCast_LIBRARY NAMES evamcast
  HINTS "${LIBMCAST_ROOT}/lib")

SET(LibMCast_LIBRARIES ${LibMCast_LIBRARY} ${LibAMCast_LIBRARY})
SET(LibMCast_INCLUDE_DIRS ${LibMCast_INCLUDE_DIR})

# handle the QUIETLY and REQUIRED arguments and set LibMCast_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LibMCast DEFAULT_MSG LibMCast_LIBRARY LibAMCast_LIBRARY LibMCast_INCLUDE_DIR)

MARK_AS_ADVANCED(LibMCast_INCLUDE_DIR LibMCast_LIBRARY LibAMCast_LIBRARY)


