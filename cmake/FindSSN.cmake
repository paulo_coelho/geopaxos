# - Find ssn
# Find the native Message Pack headers and libraries.
#
#  SSN_INCLUDE_DIRS - where to find ssn.h.
#  SSN_LIBRARIES    - List of libraries when using SSN.
#  SSN_FOUND        - True if SSN found.

# Look for the header file.
FIND_PATH(SSN_INCLUDE_DIR NAMES ssn.h
  HINTS "${SSN_ROOT}/include")

# Look for the library.

FIND_LIBRARY(SSN_LIBRARY NAMES ssn
  HINTS "${SSN_ROOT}/lib")

SET(SSN_LIBRARIES ${SSN_LIBRARY})
SET(SSN_INCLUDE_DIRS ${SSN_INCLUDE_DIR})

# handle the QUIETLY and REQUIRED arguments and set SSN_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(SSN DEFAULT_MSG SSN_LIBRARY SSN_INCLUDE_DIR)

MARK_AS_ADVANCED(SSN_INCLUDE_DIR SSN_LIBRARY)


