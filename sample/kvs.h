/*
 * Copyright (c) 2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _KVS_H_
#define _KVS_H_

#include <geopaxos/buffer.h>

#define KVS_MAX_KEY 10000
#define KVS_MAX_VALUE (MAX_DATA_SIZE - 2 * sizeof(unsigned))

// key-value store items
struct kvs_item_t
{
  unsigned key;
  unsigned len;
  char     value[KVS_MAX_VALUE];
};

struct kvs_op_t
{
  unsigned          from_group;
  struct timeval    ts;
  unsigned          type; // 0: get; 1: set
  struct kvs_item_t it;
};

enum op_type_t
{
  KVS_GET,
  KVS_SET
};

// the key-value store
struct kvs_t;

struct kvs_t*      kvs_new();
void               kvs_free(struct kvs_t* k);
struct kvs_item_t* kvs_get(struct kvs_t* k, unsigned key);
struct kvs_item_t* kvs_get_range(struct kvs_t* k, unsigned from_key,
                                 unsigned to_key, int* count);
unsigned           kvs_set(struct kvs_t* k, struct kvs_item_t* it);

#endif