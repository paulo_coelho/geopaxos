/* * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <event2/thread.h>
#include <geopaxos.h>
#include <geopaxos/log.h>
#include <getopt.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STATS_INTERVAL 5
#define MAX_SIZE 20000

struct stats
{
  long delivered;
  long min_latency;
  long max_latency;
  long avg_latency;
};

struct stats_ctrl
{
  int            value_size;
  struct stats   stats;
  struct event*  stats_ev;
  struct timeval stats_interval;
  struct timeval last_tv;
};

struct client_value
{
  struct timeval t;
  unsigned long  key;
  size_t         size;
  char           value[MAX_SIZE];
};

static int verbose = 0;

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;
  printf("Caught signal %d, exiting...\n", sig);
  event_base_loopexit(base, NULL);
}

// Returns t2 - t1 in microseconds.
static long
timeval_diff(struct timeval* t1, struct timeval* t2)
{
  long us;
  us = (t2->tv_sec - t1->tv_sec) * 1e6;
  if (us < 0)
    return 0;
  us += (t2->tv_usec - t1->tv_usec);
  return us;
}

static void
update_stats(struct stats* stats, struct timeval last)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  long lat = timeval_diff(&last, &tv);

  stats->delivered++;
  stats->avg_latency =
    stats->avg_latency + ((lat - stats->avg_latency) / stats->delivered);
  if (stats->min_latency == 0 || (lat > 0 && lat < stats->min_latency))
    stats->min_latency = lat;
  if (lat > stats->max_latency)
    stats->max_latency = lat;
}

static void
on_stats(evutil_socket_t fd, short event, void* arg)
{
  struct stats_ctrl* sc = arg;
  struct stats*      s = &(sc->stats);
  int                delta_t = STATS_INTERVAL;
  double mbps = (double)(s->delivered * sc->value_size * 8) / (1024 * 1024);

  mbps /= delta_t;
  fprintf(stderr,
          "%ld msgs/sec, %.2f Mbps, message size %d, latency min %ld us max "
          "%ld us avg %ld us\n",
          s->delivered / delta_t, mbps, sc->value_size, s->min_latency,
          s->max_latency, s->avg_latency);
  memset(s, 0, sizeof(struct stats));
  event_add(sc->stats_ev, &sc->stats_interval);
}

/*
 * Sets the destination group of the message to be geocast based on the msg
 * received from the client and returns the number of destination groups.
 */
unsigned
onclient(struct geopaxos* g, unsigned client_id, const char* client_msg,
         unsigned msg_len, unsigned short* dst, void* cb_arg)
{
  unsigned             gcount = geopaxos_group_count(g);
  struct client_value* v = (struct client_value*)client_msg;

  dst[0] = v->key % gcount;
  LOG_DEBUG("destination for key %lu is group %u", v->key, dst[0]);
  return 1;
}
/*
 * Receives the client msg to be executed after geodelivery, with information
 * about the client, the group that is executing the command and the
 * destination groups.
 * Creates the response message and returns its size in bytes.
 */
unsigned
ondeliver(struct geopaxos* g, unsigned client_id, unsigned from_group,
          unsigned short* dst, unsigned dst_len, char* client_msg,
          unsigned msg_len, char** response_msg, void* cb_arg)
{
  struct stats_ctrl* sc = cb_arg;

  LOG_DEBUG("ondeliver");
  /* HERE THE NODE IS SUPPOSED TO DO SOMETHING WITH THE RECEIVED MSG,
   * PREPARE A RESPONSE AND REPLY BACK.
   *
   * THIS SIMPLE EXAMPLE ONLY ECHOES THE RECEIVED MESSAGE BACK  */

  if (verbose)
    printf("Received message from client %d: '%.16s' with %d bytes\n",
           client_id, client_msg, msg_len);

  sc->value_size = msg_len;
  update_stats(&(sc->stats), sc->last_tv);
  gettimeofday(&(sc->last_tv), NULL);

  *response_msg = client_msg;
  return msg_len;
}

static void
dispatch(struct event_base* base, struct stats_ctrl* sc)
{
  struct event* sig;

  // statistics
  memset(sc, 0, sizeof(struct stats_ctrl));
  sc->stats_interval = (struct timeval){ STATS_INTERVAL, 0 };
  sc->stats_ev = evtimer_new(base, on_stats, sc);
  gettimeofday(&(sc->last_tv), NULL);
  event_add(sc->stats_ev, &sc->stats_interval);

  // signal handling
  sig = evsignal_new(base, SIGINT, handle_sigint, base);
  evsignal_add(sig, NULL);
  signal(SIGPIPE, SIG_IGN);
  event_base_dispatch(base);
  event_free(sc->stats_ev);
  event_free(sig);
}

static void
start_replica(int group_id, int id, const char* ip, int port,
              const char* config)
{
  struct event_base* base;
  struct geopaxos*   n;
  destination_cb     ccb = onclient;
  execute_cb         ecb = ondeliver;
  struct stats_ctrl  sc;

  base = event_base_new();
  n = geopaxos_init(group_id, id, config, base);
  if (n == NULL) {
    printf("Could not start the replica.\n");
  } else {
    geopaxos_run(n, port, ccb, ecb, &sc);
    dispatch(base, &sc);
    geopaxos_stop(n);
  }

  event_base_free(base);
}

static void
usage(const char* prog)
{
  printf("Usage: %s -r <replica-id> -g <group-id> -c <path/to/mcast.conf> [-v] "
         "[-h]\n",
         prog);
  printf("  %-30s%s\n", "-r, --replica-id", "the replica id");
  printf("  %-30s%s\n", "-g, --group-id", "the replica local group's id");
  printf("  %-30s%s\n", "-i, --ip",
         "the IP to listen to client commands (defaults to localhost)");
  printf("  %-30s%s\n", "-p, --port",
         "the port to listen to client commands (defaults to 9000)");
  printf("  %-30s%s\n", "-c, --config-file",
         "path to multicast configuration file");
  printf("  %-30s%s\n", "-v, --verbose",
         "increase verbosity (print delivered messages)");
  printf("  %-30s%s\n", "-h, --help", "output this message and exit");
  exit(EXIT_FAILURE);
}

int
main(int argc, char* argv[])
{
  int                  group_id = -1, id = -1, port = 9000;
  int                  opt = 0, idx = 0;
  const char *         config = "", *ip = "127.0.0.1";
  static struct option options[] = {
    { "replica-id", required_argument, 0, 'r' },
    { "group-id", required_argument, 0, 'g' },
    { "ip", required_argument, 0, 'i' },
    { "port", required_argument, 0, 'p' },
    { "config-file", required_argument, 0, 'c' },
    { "verbose", no_argument, 0, 'v' },
    { "help", no_argument, 0, 'h' },
    { 0, 0, 0, 0 }
  };

  while ((opt = getopt_long(argc, argv, "hvr:g:c:i:p:", options, &idx)) != -1) {
    switch (opt) {
      case 'r':
        id = atoi(optarg);
        break;
      case 'g':
        group_id = atoi(optarg);
        break;
      case 'p':
        port = atoi(optarg);
        break;
      case 'i':
        ip = optarg;
        break;
      case 'c':
        config = optarg;
        break;
      case 'v':
        verbose = 1;
        break;
      default:
        usage(argv[0]);
    }
  }

  if (group_id == -1 || id == -1 || strlen(config) == 0)
    usage(argv[0]);

  if (evthread_use_pthreads() == 0)
    start_replica(group_id, id, ip, port, config);
  else
    return EXIT_FAILURE;

  return EXIT_SUCCESS;
}
