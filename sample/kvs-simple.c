#include "kvs.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LOOPS 200

int
main()
{
  int                i, count;
  unsigned           keys[LOOPS];
  struct kvs_item_t *ret, item = {.key = 0, .value = "hello", .len = 6 };
  struct kvs_t*      kv = kvs_new();

  srand(time(NULL));
  for (i = 0; i < LOOPS; i++) {
    item.key = rand() % KVS_MAX_KEY;
    item.value[0] = 'h' + (rand() % 15) - (rand() % 7);
    kvs_set(kv, &item);
    keys[i] = item.key;
  }

  for (i = 0; i < LOOPS; i++) {
    ret = kvs_get(kv, keys[i]);
    if (ret)
      printf("%5.5u: %s\n", ret->key, ret->value);
    else
      printf("No value for key %5.5u\n", keys[i]);
  }

  ret = kvs_get_range(kv, 0, KVS_MAX_KEY, &count);
  for (i = 0; i < count; i++) {
    printf("entry %3.3d: value[%5.5u] = %s\n", (i + 1), ret[i].key,
           ret[i].value);
  }

  kvs_free(kv);
  return EXIT_SUCCESS;
}
