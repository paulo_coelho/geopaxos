/*
 * Copyright (c) 2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <geopaxos.h>
#include <getopt.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_SIZE 20000

struct client_value
{
  struct timeval t;
  unsigned long  key;
  size_t         size;
  char           value[MAX_SIZE];
};

struct client
{
  struct client_value v;
  struct geoclient*   c;
};

// client configuration parameters - default values
static int         verbose = 0;
static int         msg_size = 1024;
static unsigned    port = 9000;
static const char* ip = "127.0.0.1";

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;

  printf("Caught signal %d\n", sig);
  event_base_loopexit(base, NULL);
}

static void
random_string(char* s, const int len)
{
  int               i;
  static const char alphanum[] = "0123456789abcdefghijklmnopqrstuvwxyz";

  for (i = 0; i < len - 1; ++i)
    s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  s[len - 1] = 0;
}

static void
client_submit_value(struct client* c)
{
  gettimeofday(&c->v.t, NULL);
  c->v.key = (rand() % 0xffffffff);
  if (verbose)
    printf("Client sending message '%.16s'\n", c->v.value);

  geopaxos_client_submit(c->c, (char*)&c->v,
                         sizeof(struct client_value) - MAX_SIZE + c->v.size);
}

static void
on_connect(struct geoclient* gc, void* arg)
{
  struct client* c = arg;

  random_string(c->v.value, msg_size);
  c->v.size = msg_size;
  client_submit_value(c);
}

static void
on_read(struct geoclient* gc, const char* resp, size_t resp_len, void* ctx)
{
  struct client*       c = ctx;
  struct client_value* v = (struct client_value*)resp;

  if (resp_len) {
    if (verbose)
      printf("Client %u received %zu bytes - key %lu, value %.16s\n",
             geopaxos_client_get_uid(gc), v->size, v->key, v->value);

    if (strcmp(v->value, c->v.value))
      printf("Client %u received a value different from the one it submitted\n",
             geopaxos_client_get_uid(gc));

    client_submit_value(c);
  }
}

static struct client*
make_client(struct event_base* base)
{
  struct client* c;

  c = malloc(sizeof(struct client));
  c->c = geopaxos_client_connect(ip, port, on_connect, on_read, c, base);
  if (c->c == NULL) {
    printf("Error connecting, exiting...\n");
    free(c);
    return NULL;
  }

  return c;
}

static void
client_free(struct client* c)
{
  geopaxos_client_finish(c->c);
  free(c);
}

static void
usage(const char* prog)
{
  printf("Usage: %s -i <ip-addresss> -p <port> [-s] <msg-size> [-h] [-v]\n",
         prog);
  printf("  %-30s%s%s%s\n", "-i, --ip",
         "IP address of the replica (defaults to ", ip, ")");
  printf("  %-30s%s%u%s\n", "-p, --port", "port of the replica (defaults to ",
         port, ")");
  printf("  %-30s%s%d%s\n", "-s, --message-size",
         "size of the message in bytes (defaults to ", msg_size, " bytes)");
  printf("  %-30s%s\n", "-h, --help", "output this message and exit");
  printf("  %-30s%s\n", "-v, --verbose",
         "increase verbosity (print delivered messages)");
  exit(EXIT_FAILURE);
}

int
main(int argc, char* argv[])
{
  int                  opt = 0, idx = 0;
  struct client*       c;
  struct event_base*   base;
  struct event*        sig;
  static struct option options[] = { { "ip", required_argument, 0, 'i' },
                                     { "port", required_argument, 0, 'p' },
                                     { "message-size", required_argument, 0,
                                       's' },
                                     { "verbose", no_argument, 0, 'v' },
                                     { "help", no_argument, 0, 'h' },
                                     { 0, 0, 0, 0 } };

  while ((opt = getopt_long(argc, argv, "hvi:p:s:", options, &idx)) != -1) {
    switch (opt) {
      case 'i':
        ip = optarg;
        break;
      case 'p':
        port = atoi(optarg);
        break;
      case 's':
        msg_size = atoi(optarg);
        break;
      case 'v':
        verbose = 1;
        break;
      default:
        usage(argv[0]);
    }
  }

  srand(time(NULL));
  base = event_base_new();
  c = make_client(base);
  if (c) {
    sig = evsignal_new(base, SIGINT, handle_sigint, base);
    evsignal_add(sig, NULL);
    event_base_dispatch(base);
    client_free(c);
    event_free(sig);
  }

  event_base_free(base);
  return EXIT_SUCCESS;
}
