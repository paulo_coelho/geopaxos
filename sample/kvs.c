/*
 * Copyright (c) 2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "kvs.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct kvs_t
{
  unsigned           size;
  struct kvs_item_t* its;
};

struct kvs_t*
kvs_new()
{
  struct kvs_t* kv = malloc(sizeof(struct kvs_t));
  kv->its = malloc((1 + KVS_MAX_KEY) * sizeof(struct kvs_item_t));
  kv->size = 0;
  memset(kv->its, 0, (1 + KVS_MAX_KEY) * sizeof(struct kvs_item_t));
  return kv;
}

void
kvs_free(struct kvs_t* k)
{
  if (k) {
    free(k->its);
    free(k);
  }
}

struct kvs_item_t*
kvs_get(struct kvs_t* k, unsigned key)
{
  assert(key <= KVS_MAX_KEY);
  if (k->its[key].len)
    return &k->its[key];

  return NULL;
}

struct kvs_item_t*
kvs_get_range(struct kvs_t* k, unsigned from_key, unsigned to_key, int* count)
{
  assert(from_key <= KVS_MAX_KEY);
  assert(to_key <= KVS_MAX_KEY);

  unsigned           from = from_key < to_key ? from_key : to_key;
  unsigned           to = to_key > from_key ? to_key : from_key, i;
  struct kvs_item_t* its = malloc(sizeof(struct kvs_item_t) * (to - from + 1));

  *count = 0;
  for (i = from; i <= to; i++) {
    if (k->its[i].len)
      its[(*count)++] = k->its[i];
  }

  return its;
}

unsigned
kvs_set(struct kvs_t* k, struct kvs_item_t* it)
{
  if (it && it->len) {
    assert(it->key <= KVS_MAX_KEY);
    if (k->its[it->key].len == 0 && it->len > 0)
      k->size++;

    k->its[it->key] = *it;
    return 1;
  }

  return 0;
}

