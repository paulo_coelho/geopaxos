/*
 * Copyright (c) 2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "kvs.h"
#include <errno.h>
#include <event2/thread.h>
#include <geopaxos.h>
#include <getopt.h>
#include <inttypes.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct client
{
  struct kvs_op_t   op;
  struct geoclient* c;
};

// client configuration parameters - default values
static int         verbose = 0;
static int         msg_size = 64;
static int         threads_count = 1;
static unsigned    port = 9000;
static const char* ip = "127.0.0.1";
static unsigned    operation = 0;

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;

  printf("Caught signal %s\n", strsignal(sig));
  if (sig == SIGINT)
    event_base_loopexit(base, NULL);
}

static void
random_string(char* s, const int len)
{
  int               i;
  static const char alphanum[] = "0123456789abcdefghijklmnopqrstuvwxyz";

  for (i = 0; i < len - 1; ++i)
    s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  s[len - 1] = 0;
}

static void
client_submit_value(struct client* c)
{
  gettimeofday(&c->op.ts, NULL);
  c->op.type = operation;
  c->op.it.key = (rand() % KVS_MAX_KEY);

  switch (c->op.type) {
    case KVS_GET:
      c->op.it.len = 0;
      break;
    case KVS_SET:
      c->op.it.len = msg_size;
      break;
  }

  if (verbose)
    printf("Client sending command '%u' for key %u\n", c->op.type,
           c->op.it.key);

  geopaxos_client_submit(c->c, (char*)&c->op,
                         sizeof(struct kvs_op_t) - KVS_MAX_VALUE +
                           c->op.it.len);
}

static void
on_connect(struct geoclient* gc, void* arg)
{
  struct client* c = arg;

  random_string(c->op.it.value, msg_size);
  client_submit_value(c);
}

static void
on_read(struct geoclient* gc, const char* resp, size_t resp_len, void* ctx)
{
  struct client*   c = ctx;
  struct kvs_op_t* v = (struct kvs_op_t*)resp;

  if (resp_len) {
    if (verbose)
      printf(
        "Client %u received %u bytes for operation %u on key %u, value %.16s\n",
        geopaxos_client_get_uid(gc), v->it.len, v->type, v->it.key,
        v->it.len ? v->it.value : "'EMPTY'");

    client_submit_value(c);
  }
}

static void*
run_client(void* arg)
{
  struct client*     c;
  struct event_base* base;

  c = malloc(sizeof(struct client));
  base = event_base_new();
  c->c = geopaxos_client_connect(ip, port, on_connect, on_read, c, base);
  if (c->c == NULL) {
    printf("Error connecting, exiting...\n");
  } else {
    event_base_dispatch(base);
    geopaxos_client_finish(c->c);
    event_base_loopexit(base, NULL);
    event_base_free(base);
  }

  free(c);
  return NULL;
}

static void
usage(const char* prog)
{
  printf("Usage: %s -i <ip-addresss> -p <port> [-s] <msg-size> [-h] [-v]\n",
         prog);
  printf("  %-30s%s%s%s\n", "-i, --ip",
         "IP address of the replica (defaults to ", ip, ")");
  printf("  %-30s%s%u%s\n", "-p, --port", "port of the replica (defaults to ",
         port, ")");
  printf("  %-30s%s%d%s\n", "-s, --message-size",
         "size of the message in bytes (defaults to ", msg_size, " bytes)");
  printf("  %-30s%s\n", "-o, --operation",
         "operation to be invoked by the clients: 0 = GET, 1 = SET, 2 = GET "
         "RANGE (defaults to GET)");
  printf("  %-30s%s\n", "-t, --threads",
         "number of clients (threads) (defaults to 1)");
  printf("  %-30s%s\n", "-h, --help", "output this message and exit");
  printf("  %-30s%s\n", "-v, --verbose",
         "increase verbosity (print delivered messages)");
  exit(EXIT_FAILURE);
}

int
main(int argc, char* argv[])
{
  int                  opt = 0, idx = 0;
  struct event_base*   base;
  struct event*        sig;
  pthread_t*           t;
  static struct option options[] = { { "ip", required_argument, 0, 'i' },
                                     { "port", required_argument, 0, 'p' },
                                     { "message-size", required_argument, 0,
                                       's' },
                                     { "operation", required_argument, 0, 'o' },
                                     { "threads", required_argument, 0, 't' },
                                     { "verbose", no_argument, 0, 'v' },
                                     { "help", no_argument, 0, 'h' },
                                     { 0, 0, 0, 0 } };

  while ((opt = getopt_long(argc, argv, "hvi:p:s:o:t:", options, &idx)) != -1) {
    switch (opt) {
      case 'i':
        ip = optarg;
        break;
      case 'p':
        port = atoi(optarg);
        break;
      case 's':
        msg_size = atoi(optarg);
        break;
      case 'o':
        operation = atoi(optarg) % 2;
        break;
      case 't':
        threads_count = atoi(optarg);
        break;
      case 'v':
        verbose = 1;
        break;
      default:
        usage(argv[0]);
    }
  }

  srand(time(NULL));
  evthread_use_pthreads();
  t = malloc(threads_count * sizeof(pthread_t));

  for (idx = 0; idx < threads_count; idx++) {
    usleep(50);
    pthread_create(&t[idx], NULL, run_client, NULL);
  }

  base = event_base_new();
  sig = evsignal_new(base, SIGINT, handle_sigint, base);
  evsignal_add(sig, NULL);
  event_base_dispatch(base);
  event_free(sig);
  event_base_free(base);
  for (idx = 0; idx < threads_count; idx++) {
    pthread_join(t[idx], NULL);
  }

  free(t);
  return EXIT_SUCCESS;
}
