/*
 * Copyright (c) 2017-2018, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _GEOPAXOS_H_
#define _GEOPAXOS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <event2/buffer.h>
#include <event2/event.h>

  /**************** SERVER SIDE ***************/
  struct geopaxos;
  /*
   * Sets the destination group of the message to be geocast based on the msg
   * received from the client and returns the number of destination groups.
   */
  typedef unsigned (*destination_cb)(struct geopaxos* g, unsigned client_id,
                                     const char* client_msg, unsigned msg_len,
                                     unsigned short* dst, void* cb_arg);
  /*
   * Receives the client msg to be executed after geodelivery, with information
   * about the client, the group that is executing the command and the
   * destination groups. Creates the response message and returns its size in
   * bytes.
   */
  typedef unsigned (*execute_cb)(struct geopaxos* g, unsigned client_id,
                                 unsigned from_group, unsigned short* dst,
                                 unsigned dst_len, char* client_msg,
                                 unsigned msg_len, char** response_msg,
                                 void* cb_arg);

  /*
   * Initializes geopaxos and connects to multicast infrastructure.
   */
  struct geopaxos* geopaxos_init(unsigned my_group, unsigned my_id,
                                 const char*        libmcast_config,
                                 struct event_base* base);
  /*
   * Initializes geopaxos and connects to multicast infrastructure.
   * Specifies which node in each group the replica should connect to.
   */
  struct geopaxos* geopaxos_init2(unsigned my_group, unsigned my_id,
                                  unsigned* nids, unsigned nids_len,
                                  const char*        libmcast_config,
                                  struct event_base* base);
  /*
   * Starts listening on the specified port.
   * Uses the informed destination_cb to defined the destination groups and
   * invokes the execute_cb after message is geodelivered.
   */
  unsigned geopaxos_run(struct geopaxos* g, unsigned port, destination_cb d,
                        execute_cb e, void* cb_arg);
  /*
   * Starts listening on the specified port.
   * Uses the informed destination_cb in a thread pool (with size equals to the
   * number of groups in the system) to define the destination groups and
   * invokes the execute_cb after message is geodelivered.
   */
  unsigned geopaxos_run_parallel(struct geopaxos* g, unsigned port,
                                 destination_cb d, execute_cb e, void* cb_arg);
  /*
   * Replies to the client identified by client_id
   */
  void geopaxos_reply(struct geopaxos* g, unsigned client_id,
                      const char* response, unsigned len);
  /*
   * Closes all connections and frees allocated memory.
   */
  void geopaxos_stop(struct geopaxos* g);

  /*
   * Returns the current number of groups in the system
   */
  unsigned geopaxos_group_count(struct geopaxos* g);

  /**************** CLIENT SIDE ***************/
  struct geoclient;
  /* Callback invoked when a new message is ordered with GeoPaxos */
  typedef void (*geoclient_callback)(struct geoclient* c, const char* msg,
                                     size_t len, void* cb_arg);
  /* Callback invoked when a new connection is established successfully */
  typedef void (*geoclient_connect_callback)(struct geoclient* c, void* cb_arg);

  /*
   * Connects to a GeoPaxos server and register the callbacks
   */
  struct geoclient* geopaxos_client_connect(const char* ip, unsigned port,
                                            geoclient_connect_callback ccb,
                                            geoclient_callback cb, void* cb_arg,
                                            struct event_base* base);
  /*
   * Submits a message to GeoPaxos infrastructure
   */
  void geopaxos_client_submit(struct geoclient* c, const char* msg, size_t len);
  /*
   * Finishes connection and frees allocated memory
   */
  void geopaxos_client_finish(struct geoclient* c);
  /*
   * Returns client unique id
   */
  unsigned geopaxos_client_get_uid(struct geoclient* c);

#ifdef __cplusplus
}
#endif
#endif
